import { task } from "hardhat/config";

task("tenderly", "Persist contract in tenderly")
    .addPositionalParam("contractName", "The deployed contract name")
    .addPositionalParam("contractAddress", "The contract address")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs, hre)
        const contractAddress = taskArgs.contractAddress;
        const contractName = taskArgs.contractName;
        await hre.tenderly.persistArtifacts({
            name: contractName,
            address: contractAddress,
        })
});
