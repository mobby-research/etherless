import { task } from "hardhat/config";
import { parseUnits, formatUnits } from "ethers";

task("lock", "Locks tokens on the given wallet address")
    .addPositionalParam("contractName", "The deployed contract name")
    .addPositionalParam("contractAddress", "The contract address")
    .addPositionalParam("walletAddress", "The owner's wallet address")
    .addPositionalParam("amount", "the amount to unlock")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs, hre)
        const contractAddress = taskArgs.contractAddress;
        const contract = await hre.ethers.getContractAt(taskArgs.contractName,
            contractAddress);

        const decimals = await contract.decimals();

        const addr = taskArgs.walletAddress;
        const amt = parseUnits(taskArgs.amount, decimals);

        const res = await contract.lock(addr, amt);
        console.log(`lock(${addr}, ${formatUnits(amt, decimals)}):`, res)
});
