import fs from 'fs';
import { parse } from 'csv-parse/sync';
import { task } from 'hardhat/config';
import { formatUnits, parseUnits, isAddress, hexlify, toUtf8Bytes, toBeHex } from "ethers";

const EthCol = "Ethereum Address";
const ValueCol = "Amount";
const UnlockCol = "Unlock";

task("mint", "Mints tokens to addresses with the specified settings from a csv file")
    .addPositionalParam("contractName", "The deployed contract name")
    .addPositionalParam("contractAddress", "The contract address")
    .addPositionalParam("csv", "The csv file to parse")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs, hre)
        const contractName = taskArgs.contractName;
        const contractAddress = taskArgs.contractAddress;
        const csvPath = taskArgs.csv;

        // Get a proper contract object with the owner wallet
        const contract = await hre.ethers.getContractAt(contractName, contractAddress);
        const [ownerWallet] = await hre.ethers.getSigners();
        const listedOwner = await contract.owner();
        if (listedOwner != ownerWallet.address) {
            console.log("Listed Owner != Provided Owner", listedOwner, ownerWallet.address);
            console.log("In hardhat.config.ts, set the private key for this address: ", listedOwner);
            return -1;
        }

        const decimals = await contract.decimals();
        
        // Parse csv into contract arguments
        const input = fs.readFileSync(csvPath,'utf8');
        const records = parse(input, {
            columns: true,
            skip_empty_lines: true
        });

        // Validate each record
        let i = 0;
        type MintRecord = {
            address: string; // ethereum address
            value: bigint; // how much to mint for this address
            unlock: number; // number of seconds (from now) it should unlock
        }
        let mints = Array<MintRecord>();
        for (const r of records) {
            let newAddress = "";
            let newValue = 0n;
            let newUnlock = 0;
            if (r[EthCol] == "") {
                console.warn("Record %d empty, skipping!");
                i += 1;
                continue;
            }

            if (!isAddress(r[EthCol]) && r[EthCol] != "0x0") {
                console.error("Record %d: invalid address '%s'", i, r[EthCol]);
                throw('parsing error invalid address');
            } else {
                newAddress = r[EthCol];
            }
            try {
                newValue = parseUnits(r[ValueCol], decimals);
                newUnlock = parseInt(r[UnlockCol]);
                if (!(newValue > 0)) {
                    throw('invalid value: ' + newValue);
                }
                if (!(newUnlock >= 0)) {
                    throw('invalid unlock: ' + newUnlock);
                }
            } catch(err) {
                console.error("Record %d: could not parse integer, %s", i, err);
                throw(err);
            }

            mints.push({
                address: newAddress,
                value: newValue,
                unlock: newUnlock
            });
            i += 1;
        }

        console.log("Read %d records...", i);
        
        for (const r of mints) {
            let unlockBytes = hexlify(toUtf8Bytes("Unlocked"));
            if (r.unlock > 0) {
                unlockBytes = toBeHex(BigInt((Date.now()) + r.unlock), 32);
            }
            console.log("Minting %d tokens for %s unlocking in %s seconds", r.value, r.address, r.unlock);
            if (r.address == "0x0") {
                console.log("ZERO ADDRESS DETECTED - MINT AND BURN");
                const res = await contract.mint(ownerWallet.address, r.value, unlockBytes);
                console.log(`ZERO mint(${ownerWallet.address}, ${formatUnits(r.value, decimals)}, ${unlockBytes}):`, res);
                const burnRes = await contract.burn(ownerWallet.address, r.value, unlockBytes);
                console.log(`burn(${ownerWallet.address}, ${formatUnits(r.value, decimals)}):`, burnRes);
                continue
            }
            const res = await contract.mint(r.address, r.value, unlockBytes);
            console.log(`mint(${r.address}, ${formatUnits(r.value, decimals)}, ${unlockBytes}):`, res);
        }
});

