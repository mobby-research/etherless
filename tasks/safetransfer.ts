import fs from 'fs';
import { parse } from 'csv-parse/sync';
import { task } from 'hardhat/config';
import { formatUnits, parseUnits, isAddress, hexlify, toUtf8Bytes, toBeHex } from "ethers";
import SafeApiKit from '@safe-global/api-kit';
import Safe, { SafeTransactionOptionalProps } from '@safe-global/protocol-kit'


import hre from 'hardhat';
import { OperationType } from '@safe-global/safe-core-sdk-types';

const EthCol = "Ethereum Address";
const ValueCol = "Amount";
const UnlockCol = "Unlock";

task("safetransfer", "Creates a safe transfer batch function with the specified settings from a csv file")
    .addPositionalParam("safeAddress", "The safe wallet address to propose with")
    .addPositionalParam("contractName", "The deployed contract name")
    .addPositionalParam("contractAddress", "The contract address")
    .addPositionalParam("csv", "The csv file to parse")
    .addPositionalParam("offset", "The starting index to start processing transactions")
    .addPositionalParam("limit", "The maximum number of transactions")
    .addOptionalPositionalParam("nonce", "The nonce to use", "0")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs, hre)
        const safeAddress = taskArgs.safeAddress;
        const contractName = taskArgs.contractName;
        const contractAddress = taskArgs.contractAddress;
        const csvPath = taskArgs.csv;
        const offset = parseInt(taskArgs.offset);
        var limit = parseInt(taskArgs.limit);
        var nonce = parseInt(taskArgs.nonce);

        // Get a proper contract object with the owner wallet
        const contract = await hre.ethers.getContractAt(contractName, contractAddress);
        const [ownerWallet] = await hre.ethers.getSigners();
        const listedOwner = await contract.owner();

        if (listedOwner != safeAddress) {
            console.log("Listed ERC20 Contract Owner != Provided Owner (safe address argument)", listedOwner, safeAddress);
            return -1;
        }

        // Init the safe SDK
        const chainId = BigInt(await hre.network.provider.send('eth_chainId'));
        const apiKit = new SafeApiKit({ chainId });

        // FIXME: the following are enough to get the typescript not to complain but we need to figure out actual config
        const props = {
            provider: hre.network.provider,
            signer: process.env.PRIVATE_KEY,
            safeAddress: safeAddress
        };
        const safeSdk = await Safe.init(props);
        if (nonce === 0) {
            nonce = await safeSdk.getNonce();
        }

        const decimals = await contract.decimals();
        
        // Parse csv into contract arguments
        const input = fs.readFileSync(csvPath,'utf8');
        const records = parse(input, {
            columns: true,
            skip_empty_lines: true
        });

        // Validate each record
        let i = 0;
        type TxRecord = {
            address: string; // ethereum address
            value: bigint; // how much to mint for this address
            unlock: number; // number of seconds (from now) it should unlock
        }
        let txData = Array<TxRecord>();
        for (const r of records) {
            let newAddress = "";
            let newValue = 0n;
            let newUnlock = 0;
            if (r[EthCol] == "") {
                console.warn("Record %d empty, skipping!", i);
                i += 1;
                continue;
            }

            if (!isAddress(r[EthCol]) || r[EthCol] == "0x0") {
                console.error("Record %d: invalid address '%s'", i, r[EthCol]);
                throw('parsing error invalid address');
            } else {
                newAddress = r[EthCol];
            }
            try {
                newValue = parseUnits(r[ValueCol], decimals);
                newUnlock = parseInt(r[UnlockCol]);
                if (!(newValue > 0)) {
                    throw('invalid value: ' + newValue);
                }
                if (!(newUnlock >= -1)) {
                    throw('invalid unlock: ' + newUnlock);
                }
            } catch(err) {
                console.error("Record %d: could not parse integer, %s", i, err);
                throw(err);
            }

            txData.push({
                address: newAddress,
                value: newValue,
                unlock: newUnlock
            });
            i += 1;
        }

        console.log("Read %d records...", i);

        let txs = Array();
        let lockTxs = Array();
        var cnt = 0;
        if (limit === 0 || limit === -1) {
            limit = i
        }
        for (var j = offset; j < txData.length && cnt < limit; j++) {
            cnt += 1;

            const r = txData[j];
            console.log("txData[%d]: %s", j, r);        
            // Transfer Call
            txs.push({
                to: contractAddress,
                value: '0',
                data: contract.interface.encodeFunctionData('transfer', [
                    r.address,
                    r.value
                ]),
                operation: OperationType.Call
            });
            console.log("Record %d: Transferring %d tokens to %s", j, r.value, r.address);

            // Unlock Call - note that it might be possible to transfer tokens after transfer above but before this takes effect. 
            if (r.unlock != 0) {
                lockTxs.push({
                    to: contractAddress,
                    value: '0',
                    data: contract.interface.encodeFunctionData('lock', [
                        r.address,
                        r.value
                    ]),
                    operation: OperationType.Call
                });
                if (r.unlock > 0) {
                    const unlockTime = BigInt((Date.now()) + r.unlock)
                    lockTxs.push({
                        to: contractAddress,
                        value: '0',
                        data: contract.interface.encodeFunctionData('addTimedUnlock', [
                            r.address,
                            unlockTime,
                            r.value
                        ]),
                        operation: OperationType.Call
                    });
                }
                console.log("Record %d: Locking for %d seconds", j, r.unlock);
            }
        }

        const options: SafeTransactionOptionalProps = {
            nonce: nonce,
        };
        const batchTransaction = await safeSdk.createTransaction({ transactions: [...txs, ...lockTxs], options: options, onlyCalls: true });
        const batchTxHash = await safeSdk.getTransactionHash(batchTransaction);
        const senderSignature = await safeSdk.signHash(batchTxHash)

        // Propose the transaction
        await apiKit.proposeTransaction({
            safeAddress: safeAddress,
            safeTransactionData: batchTransaction.data,
            safeTxHash: batchTxHash,
            senderAddress: ownerWallet.address,
            senderSignature: senderSignature.data
        })

        // Lock transactions
        /*
        const lockOptions: SafeTransactionOptionalProps = {
            nonce: nonce + 1,
        };
        const batchLockTransaction = await safeSdk.createTransaction({ transactions: lockTxs, options: lockOptions, onlyCalls: true });
        const batchLockTxHash = await safeSdk.getTransactionHash(batchLockTransaction);
        const senderSignatureLock = await safeSdk.signHash(batchLockTxHash)

        // Propose the locking transactions
        await apiKit.proposeTransaction({
            safeAddress: safeAddress,
            safeTransactionData: batchLockTransaction.data,
            safeTxHash: batchLockTxHash,
            senderAddress: ownerWallet.address,
            senderSignature: senderSignatureLock.data
        })
        */
});

