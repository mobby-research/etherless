# Mobby Token

The Mobby token is an etherless, lockable token based on the OpenZeppelin Library.

To install and test this contract locally, you need nodejs installed. Then you can
run the following commands:

```shell
npm install
npx hardhat compile # compile solidity contracts
npx hardhat test # run unit tests
REPORT_GAS=true npx hardhat test # run unit tests with gas coverage
npx hardhat coverage # run code coverage tool
npx hardhat check # run linter
```

# Tasks

A number of tasks are available for you to use. To do so, you may need to set certain environment variables: 

1. PRIVATE_KEY - the private key of the address that is going to perform the action
2. ETHERSCAN_API_KEY - an etherscan api key
3. INFURA_API_KEY - an Infura API key

Check `hardhat.config.ts` for more possibilities. 

## Deploying

Configure your network, e.g., sepolia, inside of hardhat.config.ts, then run the following:

```
npx hardhat run --network sepolia scripts/deploy.ts 
```

After this deploys, in a few minutes you will be able to run the verifier with the provided contract address: 

```
npx hardhat verify --network sepolia 0x903Aa2aDd5d7B224874b6D8Ed9Be657Ca70A55C5 
```

Note that Sourcify might fail, you can run the verification for that manually at https://sourcify.dev/

## Changing Ownership

To use the change ownership task, you can run: 

```
npx hardhat --network sepolia changeOwner Mobby "0x903Aa2aDd5d7B224874b6D8Ed9Be657Ca70A55C5" "0xcD50D0D4f978281098BF5Bf6aD74b6985B424bBc"
```

The positional arguments are: 
1. The name of the contract (`Mobby`, see `scripts/deploy.ts`)
2. The contract Address from the deployment step
3. The new owner address

## Initial Mint

```
npx hardhat --network sepolia mint Mobby 0x4AbDCf1C5C4a88C5832b9Ad3350dfC1923d2079C Mōbby\ Ethereum\ Token\ -\ Data\ Collection\ \(Responses\)\ -\ Form\ Responses\ 1.csv
```

## Unlocking Tokens

```
npx hardhat --network sepolia unlock Mobby 0x4AbDCf1C5C4a88C5832b9Ad3350dfC1923d2079C 0xCFca6375C440BD79325ecD5e37aC4EaB24CB3362 5000
```

## Safe Multi-Sig Transfer

A safe multisig which owns many tokens can be used to do a group transfer. This requires a CSV with the following column fields defined in the first line of the file: 

1. `Ethereum Address` - the destination address of the tokens.
2. `Amount` - the number of tokens
3. `Unlock` - number of seconds from the current time to unlock the tokens. If set to 0 no lock call will be included. 

Example usage:

```
npx hardhat --network sepolia safetransfer 0x764d217bB2FbbB592EdCa1eDCCf8FBe6b6FCd728 Mobby 0x03d3c34709db4e89D372817feBF9fE3b344E0d85 file.csv 3 3
```

This will read the CSV, create an ordered list of records (based on which are valid, invalid records or empty lines are skipped) and submit records 3, 4, and 5 as a batch transaction to the safe wallet. 

`PRIVATE_KEY` must be set to one of the signers of the safe wallet. 