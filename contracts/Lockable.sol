// SPDX-License-Identifier: MIT
pragma solidity ^0.8.23;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev structure for storing `when` locks unlock `amount` tokens
 */
struct TimedUnlock {
    uint256 when;
    uint256 amount;
}

abstract contract Lockable is ERC20, Ownable {
    mapping(address account => uint256) private _lockedBalances;
    mapping(address account => TimedUnlock[]) private _timedUnlocks;

    // REASON_BY_OWNER means that the owner took the Lock/Unlock action
    uint256 public constant REASON_BY_OWNER = 0;
    // REASON_TIME_LOCK means that the coins are locked/unlocked for a timed unlock
    uint256 public constant REASON_TIME_LOCK = 1;
    // REASON_BURN means that the coins are being burned
    uint256 public constant REASON_BURN = 2;

    /**
     * @dev Lock event follows:
     * https://github.com/ethereum/EIPs/issues/1132
     * @param lockee the address with funds being locked
     * @param reason 0 for owner/default, 1 for time locked
     * @param amount total value being locked
     * @param validUntil block timestamp at which this lock expires, 0 for infinite
     */
    event Lock(
        address indexed lockee,
        uint256 indexed reason,
        uint256 amount,
        uint256 validUntil
    );
    /**
     * @dev Unlock event follows:
     * https://github.com/ethereum/EIPs/issues/1132
     * @param lockee the address with funds being unlocked
     * @param reason 0 for owner/default, 1 for time unlocked, 2 for burned
     * @param amount total value being unlocked
     */
    event Unlock(
        address indexed lockee,
        uint256 indexed reason,
        uint256 amount
    );

    error LockableInvalidAddress(address invalid);
    error LockableInsufficientFunds(
        address lockee,
        uint256 requested,
        uint256 available
    );
    error LockableTimeInThePast(address who, uint256 when, uint256 amount);

    /**
     * @dev prevent a users funds from transfer, return a status (restriction) code
     * @param lockee the address on which to lock the coins
     * @param amount the amount of which to lock
     *
     * `lockee` cannot be 0.
     *
     * `lockee` must have at least `amount` funds
     * unlocked and available.
     * 
     * If there are timed unlocks on this account, `processTimedUnlocks` should be 
     * called before this function to prevent spurious insufficient funds errors. 
     * We do not call it inside this function because the gas cost can be unpredictable
     * depending on how many timed unlocks might be present, and we wanted to give 
     * the user control over that.
     * 
     * NOTE: This function can be front-run. If the user sees a lock transaction 
     *       against them in the mempool, they could insert a transaction of their
     *       own where they transfer their balance to another address, perhaps to 
     *       exchange the Mōbby token with a different asset. 
     * 
     *       You should only be using this function with respect to a real-world 
     *       contract where success of the lock is required before executing the 
     *       rest of the contract. You can alternatively use a flashbot.
     *
     * On success, emits an EIP-1132 compatible `Lock` event.
     **/
    function lock(address lockee, uint256 amount) external onlyOwner {
        if (lockee == address(0)) {
            revert LockableInvalidAddress(lockee);
        }

        uint256 available = availableBalanceOf(lockee);
        if (available < amount) {
            revert LockableInsufficientFunds(lockee, amount, available);
        }

        _lockedBalances[lockee] += amount;
        emit Lock(lockee, REASON_BY_OWNER, amount, 0);
    }

    /**
     * @dev allow user to transfer funds, return a status (restriction) code
     * @param lockee the address on which to unlock the coins
     * @param amount the amount of which to unlock
     *
     * `lockee` cannot be 0.
     *
     * `lockee` must have at least `amount` funds
     * locked.
     *
     * On success, emits an EIP-1132 compatible `Unlock` event
     **/
    function unlock(address lockee, uint256 amount) external onlyOwner {
        _unlock(lockee, amount);
        emit Unlock(lockee, REASON_BY_OWNER, amount);
    }

    /**
     * @dev Destroys an `amount` of tokens of the `burnFrom` address.
     * @param burnFrom burn coins belonging to this address
     * @param amount burn this many coins
     *
     * When the token is being burned, the transfer events MUST be emitted as if
     * the token in the `amount` were transferred from the recipient address
     * identified by `burnFrom` to the address of 0x0.
     *
     * The total supply MUST decrease accordingly.
     *
     * You cannot burn `burnFrom` the address 0x0.
     *
     * Only the token owner (`msg.sender` == `burnFrom`) can burn. We explicitly do
     * not enable any etherless functionality for this operation.
     *
     * You can burn your locked balance, but your unlocked tokens burn first.
     * This will emit an unlock event.
     *
     * The tokens destroyed must be unassigned to the _balances field.
     **/
    function burn(address burnFrom, uint256 amount, bytes calldata) external {
        if (msg.sender != burnFrom || burnFrom == address(0)) {
            revert ERC20InvalidSender(burnFrom);
        }

        super._update(msg.sender, address(0), amount);

        // Detect if we burnt locked tokens, and emit appropriate unlock event
        uint256 newBalance = balanceOf(msg.sender);
        uint256 locked = _lockedBalances[msg.sender];
        if (locked > newBalance) {
            _lockedBalances[msg.sender] = newBalance;
            emit Unlock(msg.sender, REASON_BURN, locked - newBalance);
        }

        // We do not update timelocks here. When timed unlocks are processed,
        // if it's greater than the locked balance we unlock everything
        // and delete the timelocks.
    }

    /**
     * @dev add a timed unlock. This function performs an insert into a reverse-ordered array of timed
     * unlocks. If you want to minimize gas, ensure it is called to add the timed unlocks in reverse order.
     * @param who the address to add the timed unlock
     * @param when it unlocks (block timestamp, not block number)
     * @param amount how much to unlock
     *
     * Logically this function moves locked funds to timed unlock funds. As such it emits an Unlock/Lock event
     * to denote the transition.
     *
     * NOTE: The contract owner is responsible for the state of the underlying timed unlock events, and we
     *       note that there is no direct delete for timed locks as they are intended only to be used as part of
     *       minting or a specific agreement tied to a specific address. There are two intended workflows:
     *          1. Lockee has an agreement whereby they will be locked until a certain condition is met,
     *             at which time the owner unlocks the funds manually.
     *          2. Lockee has an agreement to lock up funds for a given time period, whereby funds will
     *             unlock in all or part based on the agreed time units.
     *       If an additional lock agreement is needed, funds should be moved to a new address then locked
     *       accordingly. Lock types should not be mixed and matched, unlocked and relocked, etc.
     */
    function addTimedUnlock(
        address who,
        uint256 when,
        uint256 amount
    ) external onlyOwner {
        _addTimedUnlock(who, when, amount);
        emit Unlock(who, REASON_BY_OWNER, amount);
        emit Lock(who, REASON_TIME_LOCK, amount, when);
    }

    /**
     * @dev Creates an `amount` of tokens for the `recipient` address per ERC-5679
     * @param recipient address to send the coins
     * @param amount the total amount to send
     * @param timeLockBytes if length is 32, then this indicates the block timestamp
     * at which this minted balance should be automatically unlocked.
     *
     * When the token is being minted, the transfer events MUST be emitted as if
     * the token in the `amount` were transferred from address 0x0 to the recipient
     * address identified by `recipient`.
     *
     * The total supply MUST increase accordingly.
     *
     * You cannot mint `recipient` the address 0x0.
     *
     * Only the contract owner can mint.
     *
     * The tokens must be assigned to the `_lockedBalances` and not affect the
     * _balances.
     *
     * Always emits a lock event, when timeLockBytes is specified, the emitted Lock event
     * specifies reason 1 and the validUntil field.
     */
    function mint(
        address recipient,
        uint256 amount,
        bytes calldata timeLockBytes
    ) public virtual onlyOwner {
        // Note: this emits the required Transfer event, ensures no address 0,
        //       and increases total supply
        _mint(recipient, amount);
        _lockedBalances[recipient] += amount;
        if (timeLockBytes.length == 32) {
            uint256 timeLock = uint256(abi.decode(timeLockBytes, (uint256)));
            _addTimedUnlock(recipient, timeLock, amount);
            emit Lock(recipient, REASON_TIME_LOCK, amount, timeLock);
        } else {
            emit Lock(recipient, REASON_BY_OWNER, amount, 0);
        }
    }

    /**
     * @dev execute up to `maxToProcess` timed unlocked for `who`.
     * Anyone can process timed unlocks for anyone else.
     * @param who the addres for which to process timed unlocks
     * @param maxToProcess the maximum number of timed unlocks to process (which limits how much gas you pay)
     */
    function processTimedUnlocks(
        address who,
        uint256 maxToProcess
    ) public returns (uint256 amountUnlocked) {
        uint256 curLen = _timedUnlocks[who].length;
        uint256 totalLockedAmt = _lockedBalances[who];
        uint256 amountToUnlock;
        for (uint256 i; i < maxToProcess && curLen != 0; i++) {
            // If we reach a timed unlock where the timestamp hasn't past, exit
            /* solhint-disable not-rely-on-time */
            if (block.timestamp < _timedUnlocks[who][curLen - 1].when) {
                break;
            }
            /* solhint-enable not-rely-on-time */

            // In the case of a timed unlock, if we have less locked than the amount being unlocked,
            // or our locked balance will become 0, we unlock everything left and delete the entry.
            amountToUnlock += _timedUnlocks[who][curLen - 1].amount;
            if (totalLockedAmt <= amountToUnlock) {
                amountToUnlock = totalLockedAmt;
                delete _timedUnlocks[who];
                break;
            }

            _timedUnlocks[who].pop();
            curLen -= 1;
        }
        _unlock(who, amountToUnlock);
        emit Unlock(who, REASON_TIME_LOCK, amountToUnlock);
        return amountToUnlock;
    }

    /**
     * @dev return the locked balance for a user
     * @param who the address of the balance we wish to check
     **/
    function lockedBalanceOf(
        address who
    ) public view returns (uint256 lockedBalance) {
        return _lockedBalances[who];
    }

    /**
     * @dev return the available balance for a user
     * @param who the address of the balance we wish to check
     **/
    function availableBalanceOf(
        address who
    ) public view returns (uint256 availableBalance) {
        return (balanceOf(who) - _lockedBalances[who]);
    }

    /**
     * @dev returns the TimedUnlock array for a given user
     * @param who the user to return timed unlocks for.
     */
    function timedUnlocksOf(
        address who
    ) public view returns (TimedUnlock[] memory timedUnlocks) {
        return _timedUnlocks[who];
    }

    function _unlock(address lockee, uint256 amount) internal {
        if (lockee == address(0)) {
            revert LockableInvalidAddress(lockee);
        }

        uint256 locked = _lockedBalances[lockee];
        if (locked < amount) {
            revert LockableInsufficientFunds(lockee, amount, locked);
        }

        _lockedBalances[lockee] -= amount;
    }

    function _addTimedUnlock(
        address who,
        uint256 when,
        uint256 amount
    ) internal {
        /* solhint-disable not-rely-on-time */
        if (block.timestamp > when) {
            revert LockableTimeInThePast(who, when, amount);
        }
        /* solhint-enable not-rely-on-time */

        if (amount > _lockedBalances[who]) {
            revert LockableInsufficientFunds(who, amount, _lockedBalances[who]);
        }

        // Find the index at which to insert. In the ideal case this should be length - 1 (the first one)
        uint256 origLen = _timedUnlocks[who].length;
        uint256 i;
        uint256 insertIdx;
        i = origLen;
        // Find the insertIdx location at which target timestamp is bigger than the entries timestamp
        while (i != 0) {
            i -= 1;
            if (_timedUnlocks[who][i].when > when) {
                insertIdx = i + 1;
                break;
            }
        }

        // If array was originally empty or we received an expected insert at the end of our list,
        // no need to shift units, save gas with a single push/write.
        if (insertIdx == origLen) {
            _timedUnlocks[who].push(TimedUnlock(when, amount));
            return;
        }

        // Bad insert case: enforce order by making a gap and copy until we hit the insert index,
        // then insert our value
        _timedUnlocks[who].push();
        for (i = origLen; i > insertIdx; i--) {
            _timedUnlocks[who][i] = _timedUnlocks[who][i - 1];
        }
        _timedUnlocks[who][insertIdx] = TimedUnlock(when, amount);
    }

    /**
     * @dev overrides the update function to check the available balance.
     * @param from the address from which to move funds
     * @param to the addres to which to send funds
     * @param value the amount to transfer
     */
    function _update(
        address from,
        address to,
        uint256 value
    ) internal virtual override {
        uint256 availableBalance = availableBalanceOf(from);
        if (availableBalance < value) {
            if (from != address(0) && to != address(0)) {
                availableBalance += processTimedUnlocks(
                    from,
                    type(uint256).max
                );
                if (availableBalance < value) {
                    revert ERC20InsufficientBalance(
                        from,
                        availableBalance,
                        value
                    );
                }
            }
        }
        super._update(from, to, value);
    }
}
