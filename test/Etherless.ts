import {
    time,
    loadFixture,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import { Signature, verifyTypedData } from "ethers";
import { hexlify, toUtf8Bytes } from "ethers";

export function runEtherlessTests(contractName: string) {

    async function deployEtherlessFixture() {
        const etherless = await hre.ethers.deployContract(contractName, [], {});
        const [owner, otherAccount] = await hre.ethers.getSigners();

        const domain = {
            name: await etherless.name(),
            version: "1",
            chainId: (await hre.ethers.provider.getNetwork()).chainId,
            verifyingContract: await etherless.getAddress(),
        };

        const types = {
            TransferEtherless: [
                { name: "spender", type: "address" },
                { name: "recipient", type: "address" },
                { name: "sender", type: "address" },
                { name: "value", type: "uint256" },
                { name: "fee", type: "uint256" },
                { name: "nonce", type: "uint256" },
                { name: "deadline", type: "uint256" },
            ],
        };

        if (contractName == "Mobby") {
            await etherless.mint(owner.address, 1000000n, hexlify(toUtf8Bytes("Initial Mint")));
            await etherless.unlock(owner.address, 1000000n);
        } else {
            await etherless.mint(owner.address, 1000000);
        }

        return {
            etherless,
            owner,
            otherAccount,
            domain,
            types
        };
    }

    describe("Etherless", function () {
        describe("Deployment", function () {
            it("Has 1 million owner balance", async function () {
                const { etherless, owner } = await loadFixture(deployEtherlessFixture);

                expect(await etherless.balanceOf(owner.address)).to.equal(1000000n);
            });

            it("Has empty other account balance", async function () {
                const { etherless, otherAccount } = await loadFixture(deployEtherlessFixture);

                expect(await etherless.balanceOf(otherAccount.address)).to.equal(0n);
            });

            it("Has 1million total supply", async function () {
                const { etherless } = await loadFixture(deployEtherlessFixture);

                expect(await etherless.totalSupply()).to.equal(1000000n);
            });
        });
        describe("Etherless Transactions", function () {
            it("Lets third party send to other account", async function () {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                console.log(thirdAddr);

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);


                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 500n;
                const fee = 20n;

                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = (await time.latest()) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigparts = Signature.from(signatureHex);

                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                // const valid = await verifyTypedData({
                //     address: ownerAddr,
                //     domain,
                //     types,
                //     primaryType: 'TransferEtherless',
                //     values,
                //     signatureHex,
                //   })

                var tx = await thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                );

                // Check that there is an Approval and 2 Transfer Events
                var receipt = await tx.wait();
                var ae = etherless.interface.parseLog(receipt.logs[0]);
                expect(ae.name).to.be.equal("Approval");
                expect(ae?.args[0]).to.be.equal(ownerAddr);
                expect(ae?.args[1]).to.be.equal(thirdAddr);
                expect(ae?.args[2]).to.be.equal(value+fee);
                var te1 = etherless.interface.parseLog(receipt.logs[1]);
                expect(te1.name).to.be.equal("Transfer");
                expect(te1?.args[0]).to.be.equal(ownerAddr);
                expect(te1?.args[1]).to.be.equal(thirdAddr);
                expect(te1?.args[2]).to.be.equal(fee);
                var te2 = etherless.interface.parseLog(receipt.logs[2]);
                expect(te2.name).to.be.equal("Transfer");
                expect(te2?.args[0]).to.be.equal(ownerAddr);
                expect(te2?.args[1]).to.be.equal(otherAddr);
                expect(te2?.args[2]).to.be.equal(value);


                expect(await etherless.balanceOf(otherAddr)).to.equal(value);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(fee);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n - value - fee);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce + 1n);

            });
            it("Stops third party send to other account when invalid signature", async function () {
                // We're going to do the obvious and sign as ourselves but try to transfer from the owner.
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);


                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 500n;
                const fee = 20n;

                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = (await time.latest()) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                // third party signs here, not owner. 
                const signatureHex = await thirdParty.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigparts = Signature.from(signatureHex);

                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                const valid = await verifyTypedData(
                    domain,
                    types,
                    values,
                    signatureHex,
                );

                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);

                // Make sure receiver can't do it either
                const signatureHexOther = await otherAccount.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigpartsOther = Signature.from(signatureHexOther);

                const otherEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    otherAccount,
                );

                await expect(otherEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigpartsOther.v,
                    sigpartsOther.r,
                    sigpartsOther.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);


                // And finally make sure the owner can't do it with someone elses signature
                await expect(etherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigpartsOther.v,
                    sigpartsOther.r,
                    sigpartsOther.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);

            });
            it("Fails deadline check", async function () {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);

                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 500n;
                const fee = 20n;

                const nonce = await thirdEtherless.nonces(ownerAddr);

                // Purposefully bad deadline
                const deadline = (await time.latest()) - 5000;

                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigparts = Signature.from(signatureHex);

                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                // const valid = await verifyTypedData({
                //     address: ownerAddr,
                //     domain,
                //     types,
                //     primaryType: 'TransferEtherless',
                //     values,
                //     signatureHex,
                //   })

                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith('EtherlessExpiredSignature');

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);
            });
            it("Fails when fee is greater than the balance", async function () {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);

                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 10n;
                const fee = 1000010n;

                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = (await time.latest()) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("ERC20InsufficientBalance");

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);

            });
            it("Fails when fee+value is greater than the balance", async function () {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);

                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 1000000n;
                const fee = 20n;

                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = (await time.latest()) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );

                const sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("ERC20InsufficientBalance");

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
                expect(await etherless.nonces(ownerAddr)).to.equal(nonce);
            });
            it("Fails when nonce is invalid", async function () {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);

                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );

                const value = 500n;
                const fee = 20n;

                // 1 valid tx to increase the nonce
                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = (await time.latest()) + 5000;
                var values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    sender: thirdAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };

                var signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );

                var sigparts = Signature.from(signatureHex);
                await thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )

                // Invalide nonce +1
                const noncePlusOne = await thirdEtherless.nonces(ownerAddr) + 1n;
                values.nonce = noncePlusOne
                signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
                sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("EtherlessInvalidSigner");

                // Invalide nonce -1, i.e. Reused nonce
                const nonceMinusOne = await thirdEtherless.nonces(ownerAddr) - 1n;
                values.nonce = nonceMinusOne
                signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
                sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("EtherlessInvalidSigner");

                // assert proper balances at the end (1 good tx)
                expect(await etherless.balanceOf(otherAddr)).to.equal(500n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(20n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n - 520n);
            });
    });
    it("Ensures only the intended sender can call it", async function () {
            const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);

            const [owner, otherAccount, thirdParty, fourthParty] = await hre.ethers.getSigners();
            const otherAddr = otherAccount.address;
            const ownerAddr = owner.address;
            const thirdAddr = thirdParty.address;

            console.log(thirdAddr);

            expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
            expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
            expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);


            const thirdEtherless = new hre.ethers.Contract(
                await etherless.getAddress(),
                etherless.interface,
                thirdParty,
            );

            const value = 500n;
            const fee = 20n;

            const nonce = await thirdEtherless.nonces(ownerAddr);
            const deadline = (await time.latest()) + 5000;
            const values = {
                spender: ownerAddr,
                recipient: otherAddr,
                sender: thirdAddr, 
                value: value,
                fee: fee,
                nonce: nonce,
                deadline: deadline,
            };

            const signatureHex = await owner.signTypedData(
                domain,
                types,
                values,
            );

            const sigparts = Signature.from(signatureHex);

            // NOTE: this fails with 
            // "TypeError: Cannot read properties of undefined (reading 'spender')"
            // Appears to be a bug in the library...
            // const valid = await verifyTypedData({
            //     address: ownerAddr,
            //     domain,
            //     types,
            //     primaryType: 'TransferEtherless',
            //     values,
            //     signatureHex,
            //   })

            // At this point, the signature is valid, but we are going
            // to send from a fourth wallet to simulate a third party intercepting
            // the transaction

            const fourthEtherless = new hre.ethers.Contract(
                await etherless.getAddress(),
                etherless.interface,
                fourthParty,
            );


            await expect(fourthEtherless.transferEtherless(
                ownerAddr,
                otherAddr,
                value,
                fee,
                deadline,
                sigparts.v,
                sigparts.r,
                sigparts.s,
            )).to.be.rejectedWith('EtherlessInvalidSigner');

            expect(await etherless.balanceOf(otherAddr)).to.equal(0);
            expect(await etherless.balanceOf(thirdAddr)).to.equal(0);
            expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
            expect(await etherless.nonces(ownerAddr)).to.equal(nonce);

        });
    });
}

runEtherlessTests("EtherlessTester");