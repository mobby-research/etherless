import {
    time,
    loadFixture,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import { Signature, verifyTypedData } from "ethers";
import { hexlify, toUtf8Bytes } from "ethers";

import { runLockableTests } from "./Lockable";
import { runEtherlessTests } from "./Etherless";


export function runMobbyTests(contractName: string) {

    async function deployMobbyFixture() {
        const mobby = await hre.ethers.deployContract(contractName, [], {});
        const [owner, otherAccount] = await hre.ethers.getSigners();

        const domain = {
            name: await mobby.name(),
            version: "1",
            chainId: (await hre.ethers.provider.getNetwork()).chainId,
            verifyingContract: await mobby.getAddress(),
        };

        const types = {
            TransferEtherless: [
                { name: "spender", type: "address" },
                { name: "recipient", type: "address" },
                { name: "sender", type: "address" },
                { name: "value", type: "uint256" },
                { name: "fee", type: "uint256" },
                { name: "nonce", type: "uint256" },
                { name: "deadline", type: "uint256" },
            ],
        };

        return {
            mobby,
            owner,
            otherAccount,
            domain,
            types
        };
    }

    describe("LockedEtherless", function () {
        it("Lets owner send on behalf of locked account", async function () {
            const { mobby, domain, types } = await loadFixture(deployMobbyFixture);

            const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
            const otherAddr = otherAccount.address;
            const ownerAddr = owner.address;
            const thirdAddr = thirdParty.address;

            expect(await mobby.balanceOf(otherAddr)).to.equal(0n);
            expect(await mobby.balanceOf(thirdAddr)).to.equal(0n);
            expect(await mobby.balanceOf(ownerAddr)).to.equal(0n);

            await mobby.mint(ownerAddr, 1000000n, hexlify(toUtf8Bytes("Initial Mint")));

            const thirdEtherless = new hre.ethers.Contract(
                await mobby.getAddress(),
                mobby.interface,
                thirdParty,
            );

            const value = 500n;
            const fee = 20n;

            const nonce = await thirdEtherless.nonces(ownerAddr);
            const deadline = (await time.latest()) + 5000;
            const values = {
                spender: ownerAddr,
                recipient: otherAddr,
                sender: ownerAddr,
                value: value,
                fee: fee,
                nonce: nonce,
                deadline: deadline,
            };

            const signatureHex = await owner.signTypedData(
                domain,
                types,
                values,
            );

            const sigparts = Signature.from(signatureHex);

            // NOTE: this fails with 
            // "TypeError: Cannot read properties of undefined (reading 'spender')"
            // Appears to be a bug in the library...
            // const valid = await verifyTypedData({
            //     address: ownerAddr,
            //     domain,
            //     types,
            //     primaryType: 'TransferEtherless',
            //     values,
            //     signatureHex,
            //   })

            await expect(mobby.transferEtherless(
                ownerAddr,
                otherAddr,
                value,
                fee,
                deadline,
                sigparts.v,
                sigparts.r,
                sigparts.s,
            )).to.be.rejectedWith("ERC20InsufficientBalance");

            // Now try locked version
            await expect(thirdEtherless.transferLockedEtherless(
                ownerAddr,
                otherAddr,
                value,
                fee,
                deadline,
                sigparts.v,
                sigparts.r,
                sigparts.s,
            )).to.be.rejectedWith("OwnableUnauthorizedAccount");

            expect(await mobby.balanceOf(otherAddr)).to.equal(0n);
            expect(await mobby.balanceOf(thirdAddr)).to.equal(0n);
            expect(await mobby.balanceOf(ownerAddr)).to.equal(1000000n);

            // Now try from the owner account
            await mobby.transferLockedEtherless(
                ownerAddr,
                otherAddr,
                value,
                fee,
                deadline,
                sigparts.v,
                sigparts.r,
                sigparts.s,
            );

            expect(await mobby.balanceOf(otherAddr)).to.equal(value);
            expect(await mobby.balanceOf(thirdAddr)).to.equal(0n);
            expect(await mobby.balanceOf(ownerAddr)).to.equal(1000000n - value);

        });
    });
}

describe("Mobby", function () {
    const MobbyContract = "Mobby";
    runLockableTests(MobbyContract);
    runEtherlessTests(MobbyContract);
    runMobbyTests(MobbyContract);
});