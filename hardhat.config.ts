import { HardhatUserConfig } from "hardhat/config";

import "@nomicfoundation/hardhat-toolbox";
import "@nomicfoundation/hardhat-ethers";
import "hardhat-ignore-warnings";
import "hardhat-exposed";
import "solidity-coverage";

import "@nomiclabs/hardhat-solhint";

require('hardhat-abi-exporter');

// Force DEBUG when running coverage
// This will turn off the solc optimizer
// NOTE: Test will still run with the optimizer turned on, which can break stack traces.
//       You can run coverage instead to get better debugging info or use:
//       DEBUG=TRUE npx hardhat test
if (process.argv.includes("coverage") || process.argv.includes("test")) {
  process.env.DEBUG = "TRUE"
}

// Go to https://infura.io, sign up, create a new API key
// in its dashboard, and replace "KEY" with it
let INFURA_API_KEY = process.env.INFURA_API_KEY;

// Replace this private key with your Sepolia account private key
// To export your private key from Coinbase Wallet, go to
// Settings > Developer Settings > Show private key
// To export your private key from Metamask, open Metamask and
// go to Account Details > Export Private Key
// Beware: NEVER put real Ether into testing accounts
let PRIVATE_KEY = process.env.PRIVATE_KEY;

// SEPOLIA_API_KEY is the etherscan api key and is only required for etherscan verification. 
// If not set user will get an appropriate error message instructing them on what to do.
let ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY;


// Only run these checks when the sepolia network is being used
if (process.argv.includes("sepolia")) {
  if (typeof(INFURA_API_KEY) !== "string") {
    console.log("WARNING: INFURA_API_KEY undefined, specify with: \nexport INFURA_API_KEY=\"...\"");
  }

  if (typeof(PRIVATE_KEY) !== "string") {
    console.log("WARNING: SEPOLIA_PRIVATE_KEY undefined, specify with: \nexport SEPOLIA_PRIVATE_KEY=\"...\"");
  }
}

const config: HardhatUserConfig = {
  solidity: "0.8.23",
};

require("./tasks/changeOwner.ts");
require("./tasks/mint.ts");
require("./tasks/lock.ts");
require("./tasks/unlock.ts");
require("./tasks/safetransfer.ts");

if (process.env.TENDERLY) {
  console.log("WARNING: TENDERLY ENABLED, tests may fail");
  import("@tenderly/hardhat-tenderly");
  require("./tasks/tenderly.ts");
}

module.exports = {  
  solidity: {
    version: "0.8.23",
    settings: {
      viaIR: process.env.DEBUG ? false : true,
      optimizer: {
        enabled: process.env.DEBUG ? false : true,
        details: process.env.DEBUG ? {} : {
          yulDetails: { // These are the defaults, we toyed with changes and they were all worse.
            optimizerSteps: "dhfoD[xarrscLMcCTU]uljmul:fDnTOcmu", // https://hardhat.org/hardhat-runner/docs/reference/solidity-support
          },
        },
        runs: 200,
      },
      outputSelection: { '*': { '*': ['storageLayout'] } },
    },
  },
  networks: {
    sepolia: {
      url: `https://sepolia.infura.io/v3/${INFURA_API_KEY}`,
      accounts: [PRIVATE_KEY ? PRIVATE_KEY : "deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef"]
    },
    mainnet: {
      url: `https://mainnet.infura.io/v3/${INFURA_API_KEY}`,
      accounts: [PRIVATE_KEY ? PRIVATE_KEY : "deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef"]
    },
    arbitrumSepolia: {
      url: 'https://sepolia-rollup.arbitrum.io/rpc',
      chainId: 421614,
      accounts: [PRIVATE_KEY ? PRIVATE_KEY : "deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef"]
    },
    arbitrumOne: {
      url: 'https://arb1.arbitrum.io/rpc',
      //accounts: [ARBITRUM_MAINNET_TEMPORARY_PRIVATE_KEY]
    },
  },
  sourcify: {
    enabled: true
  },
  etherscan: {
    apiKey: {
      sepolia: [ETHERSCAN_API_KEY],
      mainnet: [ETHERSCAN_API_KEY]
    }
  }
};



export default config;
